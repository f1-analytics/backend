class DatabaseRouter:
    f1_data_db_name = 'f1_data'
    f1_data_app_name = 'f1_data'

    def db_for_read(self, model, **_hints):
        if model._meta.app_label == self.f1_data_app_name:
            return self.f1_data_db_name
        return 'default'

    def db_for_write(self, model, **_hints):
        if model._meta.app_label == self.f1_data_app_name:
            return self.f1_data_db_name
        return 'default'

    def allow_relation(self, obj1, obj2, **_hints):
        return (
            obj1._meta.app_label == self.f1_data_app_name
            or obj2._meta.app_label == self.f1_data_app_name
            or self.f1_data_app_name not in [obj1._meta.app_label, obj2._meta.app_label]
        )

    def allow_migrate(self, database_name, app_label, _model_name=None, **_hints):
        if app_label == self.f1_data_app_name:
            return database_name == self.f1_data_db_name
        return None
