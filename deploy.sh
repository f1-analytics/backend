# Load env variables
source env.list

# Setup docker
sudo yum update -y
sudo amazon-linux-extras install docker
sudo service docker start
sudo usermod -a -G docker ec2-user
sudo chkconfig docker on

# Make sure databases exist and F1-data is up to date
sudo yum install mysql -y
mysql -h $DATABASE_HOST -u $DATABASE_USERNAME -p"$DATABASE_PASSWORD" -e "CREATE DATABASE IF NOT EXISTS f1_analytics;"
mysql -h $DATABASE_HOST -u $DATABASE_USERNAME -p"$DATABASE_PASSWORD" -e "CREATE DATABASE IF NOT EXISTS f1_data;"
rm -rf database.sql.gz
wget -O database.sql.gz http://ergast.com/downloads/f1db.sql.gz
gunzip < database.sql.gz | mysql -h $DATABASE_HOST -u $DATABASE_USERNAME -p"$DATABASE_PASSWORD" f1_data

# Start docker service
sudo docker login -u gitlab-ci-token -p $REPOSITORY_ACCESS_TOKEN registry.gitlab.com
sudo docker pull registry.gitlab.com/f1-analytics/backend
sudo docker rm $(sudo docker ps -q --filter=label=application=backend) --force
sudo docker run -p 8000:8000 \
    --env DATABASE_PASSWORD=$DATABASE_PASSWORD \
    --env DATABASE_USERNAME=$DATABASE_USERNAME \
    --env DATABASE_HOST=$DATABASE_HOST \
    --label application=backend \
    -d registry.gitlab.com/f1-analytics/backend \
    sh -c "python manage.py migrate; python manage.py runserver 0.0.0.0:8000"

