resource "aws_acm_certificate" "cert" {
  domain_name = var.domain_name
  validation_method = "DNS"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "cert_validation" {
  allow_overwrite = true
  name = tolist(aws_acm_certificate.cert.domain_validation_options)[0].resource_record_name
  records = [ tolist(aws_acm_certificate.cert.domain_validation_options)[0].resource_record_value ]
  type = tolist(aws_acm_certificate.cert.domain_validation_options)[0].resource_record_type
  zone_id = data.terraform_remote_state.frontend_state.outputs.hosted_zone_id
  ttl = 60
}

# This tells terraform to cause the route53 validation to happen
resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [ aws_route53_record.cert_validation.fqdn ]
}

resource "aws_route53_record" "cloudfront_record" {
  allow_overwrite = true
  name = var.domain_name
  type = "A"
  zone_id = data.terraform_remote_state.frontend_state.outputs.hosted_zone_id

  alias {
    evaluate_target_health = false
    name = aws_cloudfront_distribution.cloudfront.domain_name
    zone_id = aws_cloudfront_distribution.cloudfront.hosted_zone_id
  }
}
