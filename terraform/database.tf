resource "aws_security_group" "database_security_group" {
  ingress {
    description = "Allow database connection"
    from_port = 3306
    to_port = 3306
    protocol = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_db_instance" "main" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  parameter_group_name = "default.mysql5.7"
  username = var.database_username
  password = var.database_password
  vpc_security_group_ids = [aws_security_group.database_security_group.id]
}