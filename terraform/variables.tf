variable "domain_name" {
  type = string
  default = "api.f1analytics.ml"
}

variable "database_username" {
  type = string
}

variable "database_password" {
  type = string
}
