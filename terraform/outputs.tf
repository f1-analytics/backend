output "instance_public_ip_address" {
  value = aws_instance.instance.public_dns
}

output "db_hostname" {
  value = aws_db_instance.main.address
  sensitive = true
}

output "db_username" {
  value = aws_db_instance.main.username
  sensitive = true
}

output "db_password" {
  value = aws_db_instance.main.password
  sensitive = true
}
