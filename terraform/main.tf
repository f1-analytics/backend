terraform {
  backend "s3" {
    region = "us-east-1"
    bucket = "tf-states-backend"
    key = "backend.tfstate"
  }
}

resource "aws_security_group" "instance_security_group" {
  ingress {
    description = "Allow ssh access"
    from_port = 22
    to_port = 22
    protocol = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow Django server access"
    from_port = 8000
    to_port = 8000
    protocol = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Allow all outgoing access"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "instance" {
  ami = "ami-0947d2ba12ee1ff75"
  instance_type = "t3.micro"
  key_name = "instance_pair"
  security_groups = [aws_security_group.instance_security_group.name]
}
