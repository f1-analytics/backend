data "terraform_remote_state" "frontend_state" {
  backend = "s3"

  config = {
    bucket = "tf-states-backend"
    key = "frontend.tfstate"
    region = "us-east-1"
  }
}