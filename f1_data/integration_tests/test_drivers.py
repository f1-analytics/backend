import json
from typing import Dict
from django.test import TransactionTestCase
from model_mommy import mommy
from rest_framework.test import APIClient
from rest_framework import status
from f1_data.models import Drivers


class DriversIntegrationTestCase(TransactionTestCase):
    databases = '__all__'

    @classmethod
    def setUpClass(cls) -> None:
        super(DriversIntegrationTestCase, cls).setUpClass()
        cls.api_client = APIClient()

    @staticmethod
    def _get_expected_driver_response(driver: Drivers) -> Dict:
        return {
            'id': str(driver.driverid),
            'forename': driver.forename,
            'surname': driver.surname,
        }

    def test_getting_driver_list(self):
        driver_1 = mommy.make(Drivers)
        driver_2 = mommy.make(Drivers)

        response = self.api_client.get('/data/drivers')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertCountEqual(json.loads(response.content), [
            self._get_expected_driver_response(driver_1),
            self._get_expected_driver_response(driver_2),
        ])
