from django.urls import path

from f1_data.drivers import views

urlpatterns = [
    path('drivers', views.get_drivers_view, name='drivers')
]
