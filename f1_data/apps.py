from django.apps import AppConfig


class F1DataAppConfig(AppConfig):
    name = 'f1_data'
    verbose_name = 'F1 Data App'
