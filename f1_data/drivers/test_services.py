from unittest.mock import patch
from django.test import TestCase
from f1_data.drivers.services import get_driver_list
from f1_data.drivers.dto import Driver


class GetDriverListTestCase(TestCase):
    @patch('f1_data.drivers.services.get_drivers')
    def test_getting_driver_list(self, patched_get_drivers):
        mocked_driver_1 = Driver(
            id='1',
            forename='Fernando',
            surname='Alonso',
        )
        mocked_driver_2 = Driver(
            id='2',
            forename='Mark',
            surname='Webber',
        )
        patched_get_drivers.return_value = [mocked_driver_1, mocked_driver_2]

        actual = get_driver_list()

        self.assertEqual(actual, [mocked_driver_1, mocked_driver_2])
