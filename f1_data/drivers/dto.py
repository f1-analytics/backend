from dataclasses import dataclass


@dataclass
class Driver:
    id: str
    forename: str
    surname: str
