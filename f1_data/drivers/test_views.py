import json
from unittest.mock import patch
from typing import Dict
from django.test import TestCase
from django.test.client import RequestFactory
from f1_data.drivers.views import get_drivers_view
from f1_data.drivers.dto import Driver


class GetDriversTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.mocked_get_request = RequestFactory().get('/')

    @staticmethod
    def _get_expected_driver(mocked_driver: Driver) -> Dict:
        return {
            'id': mocked_driver.id,
            'forename': mocked_driver.forename,
            'surname': mocked_driver.surname,
        }

    @patch('f1_data.drivers.views.get_driver_list')
    def test(self, patched_execute_script):
        mocked_driver_1 = Driver(
            id='1',
            forename='Fernando',
            surname='Alonso',
        )
        mocked_driver_2 = Driver(
            id='2',
            forename='Mark',
            surname='Webber',
        )
        patched_execute_script.return_value = [mocked_driver_1, mocked_driver_2]

        actual = get_drivers_view(self.mocked_get_request)

        self.assertEqual(json.loads(actual.content), [
            self._get_expected_driver(mocked_driver_1),
            self._get_expected_driver(mocked_driver_2),
        ])
