from django.http import JsonResponse, HttpRequest
from drf_spectacular.utils import extend_schema
from rest_framework.decorators import api_view
from rest_framework import status
from f1_data.drivers.serializers import DriverSerializer
from f1_data.drivers.services import get_driver_list


@extend_schema(responses={status.HTTP_200_OK: DriverSerializer(many=True)})
@api_view(['GET'])
def get_drivers_view(_request: HttpRequest) -> JsonResponse:
    return JsonResponse(DriverSerializer(get_driver_list(), many=True).data, safe=False)
