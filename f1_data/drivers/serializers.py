from rest_framework import serializers


class DriverSerializer(serializers.Serializer):
    id = serializers.CharField()
    forename = serializers.CharField()
    surname = serializers.CharField()
