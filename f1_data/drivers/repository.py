from typing import List
from f1_data.sql import execute_script
from f1_data.drivers.dto import Driver


def get_drivers() -> List[Driver]:
    return [Driver(
        id=driver.driverId,
        forename=driver.forename,
        surname=driver.surname,
    ) for driver in execute_script('SELECT * FROM drivers ORDER BY surname')]
