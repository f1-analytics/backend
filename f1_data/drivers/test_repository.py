from unittest.mock import patch, Mock
from django.test import TestCase
from f1_data.drivers.repository import get_drivers
from f1_data.drivers.dto import Driver


class GetDriversTestCase(TestCase):
    @staticmethod
    def _get_expected_driver(mocked_driver: Mock) -> Driver:
        return Driver(
            id=mocked_driver.driverId,
            forename=mocked_driver.forename,
            surname=mocked_driver.surname,
        )

    @patch('f1_data.drivers.repository.execute_script')
    def test_getting_driver_list(self, patched_execute_script):
        mocked_driver_1 = Mock(
            driverId='1',
            forename='Fernando',
            surname='Alonso',
        )
        mocked_driver_2 = Mock(
            driverId='2',
            forename='Mark',
            surname='Webber',
        )
        patched_execute_script.return_value = [mocked_driver_1, mocked_driver_2]

        actual = get_drivers()

        self.assertEqual(actual, [
            self._get_expected_driver(mocked_driver_1),
            self._get_expected_driver(mocked_driver_2),
        ])
