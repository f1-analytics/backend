from typing import List
from f1_data.drivers.repository import get_drivers
from f1_data.drivers.dto import Driver


def get_driver_list() -> List[Driver]:
    return get_drivers()
