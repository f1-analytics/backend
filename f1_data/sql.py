from typing import Tuple, List, Any
from collections import namedtuple
from django.db import connections


def execute_script(script: str) -> List[Any]:
    with connections['f1_data'].cursor() as cursor:
        cursor.execute(script)
        return named_tuple_fetchall(cursor)


def named_tuple_fetchall(cursor: Any) -> List[Tuple]:
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]
